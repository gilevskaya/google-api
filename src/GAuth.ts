import { google } from 'googleapis';
import logger from 'typed-logger';

export class GAuth {
  public auth = '';
  public jwtClient: any;

  // configure a JWT auth client
  public async JWT(clientEmail: string, privateKey: string): Promise<GAuth> {
    return new Promise<GAuth>(async resolve => {
      const jwtClient = new google.auth.JWT(clientEmail, undefined, privateKey, [
        'https://www.googleapis.com/auth/spreadsheets'
      ]);
      // authenticate request
      const authRes = await jwtClient.authorize();
      if (authRes) {
        this.auth = 'jwtClient';
        this.jwtClient = jwtClient;
        logger.trace('Connected to GoogleAPI');
        return resolve(this);
      }
    });
  }
}
