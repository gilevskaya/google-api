import { GAuth } from '.';
// import logger from 'typed-logger';

export async function connectToGoogleAPI(): Promise<GAuth> {
  const gAuth = new GAuth();
  if (!process.env.GOOGAPI_CLIENT_EMAIL || !process.env.GOOGAPI_PRIVATE_KEY)
    throw new Error('Missing required ENV variables');
  return gAuth.JWT(process.env.GOOGAPI_CLIENT_EMAIL, process.env.GOOGAPI_PRIVATE_KEY);
}

describe('GAuth tests', () => {
  test('Connect to Google', async () => {
    expect.assertions(1);
    const gAuth = await connectToGoogleAPI();
    expect(gAuth).toBeDefined();
  });
});
