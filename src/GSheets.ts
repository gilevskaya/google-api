import { google } from 'googleapis';
import { GAuth } from './GAuth';

export class GSheets {
  constructor(private gAuth: GAuth) {
    if (!gAuth) throw new Error('Need to auth first');
  }

  public async read(spreadsheetId: string, range: string): Promise<string[][]> {
    return new Promise<string[][]>(async (resolve, reject) => {
      const gsheets = google.sheets('v4');
      const response = await gsheets.spreadsheets.values.get({
        auth: this.gAuth.jwtClient,
        spreadsheetId,
        range
      });
      if (!response || !response.data.values) return reject(`Can't get data`);
      return resolve(response.data.values);
    });
  }
}

/////////////////////////////////////////
// import logger from 'typed-logger';

// export async function connectToGoogleAPI(): Promise<GAuth> {
//   const gAuth = new GAuth();
//   if (!process.env.GOOGAPI_CLIENT_EMAIL || !process.env.GOOGAPI_PRIVATE_KEY)
//     throw new Error('Meh.');
//   return gAuth.JWT(process.env.GOOGAPI_CLIENT_EMAIL, process.env.GOOGAPI_PRIVATE_KEY);
// }

// export async function getSampleSheetData(gAuth: GAuth): Promise<string[][]> {
//   const gSheets = new GSheets(gAuth);
//   logger.debug({ gSheets });
//   const sheetId = '1IUuZWlY-7_sM7RV3J6QYn2d3b4x7kQaVl5e0vSpzCb0';
//   const sheetInfo = 'durable!A1:B2';
//   return gSheets.read(sheetId, sheetInfo);
// }

// async function test() {
//   const gAuth = await connectToGoogleAPI();
//   const dataRes = await getSampleSheetData(gAuth);
//   logger.debug({ dataRes });
// }

// test();
