import { GAuth, GSheets } from '.';
import { connectToGoogleAPI } from './GAuth.test';

export async function getSampleSheetData(gAuth: GAuth): Promise<string[][]> {
  const gSheet = new GSheets(gAuth);
  const sheetId = '1IUuZWlY-7_sM7RV3J6QYn2d3b4x7kQaVl5e0vSpzCb0';
  const sheetRange = 'durable!A1:B2';
  return gSheet.read(sheetId, sheetRange);
}

describe('GSheets tests', () => {
  let gAuth: GAuth;

  beforeEach(async () => {
    gAuth = await connectToGoogleAPI();
  });

  test('Get GSheet Data', async () => {
    expect.assertions(1);
    const sampleDataRes = await getSampleSheetData(gAuth);
    expect(sampleDataRes).toEqual([['101']]);
  });
});
